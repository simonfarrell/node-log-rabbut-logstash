var log4js = require("log4js");

log4js.addLayout('json', function(config) {
  return function(logEvent) { return JSON.stringify(logEvent) + config.separator; }
});

log4js.configure({
  appenders: { 
    local: { type: "file", filename: "local.log" },
    mq: {
      type: '@log4js-node/rabbitmq',
      host: process.env.RABBIT_HOST,
      port: process.env.RABBIT_PORT,
      username: 'guest',
      password: 'guest',
      routing_key: 'logs',
      exchange: 'e-logs',
      mq_type: 'direct',
      durable: false,
      layout: { type: 'json', separator: ',' } 
    }
  },
  categories: { default: { appenders: ["local", "mq"], level: "error" } }
});
var logger = log4js.getLogger();

setInterval(()=> {
  logger.error("Some debug messages");
}, 10000)

