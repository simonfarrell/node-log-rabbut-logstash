Basic assumption here is that RabbitMQ is writable in OSS and that logstash runs in the IT LAN, subscribing to a RabbitMQ queue. 

The files in the top directory give an example of how to configure node.js logging to write a RabbitMQ queue.

Files in the logstash directory are an example logstash config (ensure the rabbitmq plugin is installed by entering ```bin/logstash-plugin list``` from the logstash base directory in the CLI)
